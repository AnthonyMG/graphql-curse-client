import gql from 'graphql-tag'

export const CLIENTES_QUERY = gql`
query getClientes($limite : Int , $offset : Int){
    getClientes(limite : $limite ,offset : $offset){
      id,
      nombre,
      apellido,
      empresa
    }
    totalClientes
}
`
export const CLIENTE_QUERY = gql `
  query getCliente($id : String!){    
	getCliente(id :$id ){
    id,
    nombre,
    apellido,
    empresa ,
    emails {
      email
    }
    edad,
    tipo
  }	  
}
`
export const PRODUCTOS_QUERY = gql`
 query obtenerProductos($limite : Int , $offset : Int){
    obtenerProductos(limite : $limite , offset : $offset){
    id,
    nombre,
    precio,
    stock
  }  
}
`