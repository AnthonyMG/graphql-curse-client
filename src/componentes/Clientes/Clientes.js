import React , {Fragment , Component} from 'react';
import { Query , Mutation } from 'react-apollo'
import { Link } from 'react-router-dom'
import {ELIMINAR_CLIENTE} from '../../mutations'
import {CLIENTES_QUERY} from '../../queries/index'
import Paginador from '../Paginador';
 
class Clientes extends Component {
    state = {
        paginador : {
            offset : 0,
            actual :  1,
        }
    }
    limite = 10
    handleClickPaginaSiguiente = () =>{
        this.setState({
            paginador:{
            offset : this.state.paginador.offset + this.limite,
            actual : this.state.paginador.actual + 1 
            }
        })
    }
    handleClickPaginaAnterior = () =>{
        this.setState({
            paginador:{
            offset : this.state.paginador.offset - this.limite,
            actual : this.state.paginador.actual - 1 
            }
        })
    }

    render(){
        const actual = this.state.paginador.actual
       
        return(
            <Query  query = {CLIENTES_QUERY} pollInterval = {1000} variables = { {limite: this.limite , offset : this.state.paginador.offset }}>
            {({loading, error, data , startPolling , stopPolling})=>{
                if(loading) return 'Cargando...'
                if(error) return `Error ${error.message}`
                console.log(data)
                return (
                    <Fragment>                
                        <h2 className = "text-center"> Listado clientes</h2>
                        <ul className = 'list-gruop'>
                            {data.getClientes.map(item => (
                                <li key = {item.id} className = "list-group-item">
                                    <div className = "row justify-content-betweem algin-items-center">
                                        <div className = "col-md-8 d-flex justify-content-between ">
                                                {item.nombre} {item.apellido}
                                        </div>
                                        <div className= "col-nd-4 d-flex justify-content-end">
                                            <Mutation mutation = {ELIMINAR_CLIENTE}>
                                            {eliminarCliente => {
                                                    const {id} = item

                                                    return (<button 
                                                        type = "button" 
                                                        className = "btn btn-danger d-block d-md-inline-block mr-2"
                                                        onClick = {()=>{
                                                            if(window.confirm('Seguro que deseas eliminar este cliente'))
                                                                eliminarCliente({
                                                                    variables : {
                                                                        id
                                                                    }
                                                                })
                                                        }}             
                                                        >
                                                            
                                                        &times; Eliminar
                                                    </button>)  
                                            }}                                          
                                            </Mutation>
                                            <Link to = {`/cliente/editar/${item.id}`} className = "btn btn-success d-block d-nd-inline-block">
                                                Editar Cliente
                                            </Link>
                                        </div>
                                    </div>
                                </li>
                            ))}
                        </ul>
                        <Paginador 
                        actual = {actual} 
                        totalClientes = {data.totalClientes}
                        limite = {this.limite}                    
                        paginaSiguiente = {this.handleClickPaginaSiguiente}
                        paginaAnterior = {this.handleClickPaginaAnterior}
                        />
                    </Fragment>

                )
            }}
        </Query>
        )
    }
}
export default Clientes